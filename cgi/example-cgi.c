/**
 Ejemplo básico de CGI
 */

/** Incluye la biblioteca estándar para entrada-salida */
#include <stdio.h>

/**
 Define la cadena con la que terminan las líneas de cabecera y cuerpo de HTTP
 - '\r' es un retorno de línea (ASCII `0x0D`)
 - '\n' es un caracter de nueva línea (ASCII `0x0A`)
 */
#define LINE_END "\r\n"

/**
 Imprime una cabecera HTTP seguida de la cadena 'LINE_END'
 */
int header(char* name, char* value)
{
  return printf("%s: %s%s", name, value, LINE_END);
}

/**
 Imprime un mensaje en el cuerpo de la respuesta seguido de la cadena 'LINE_END'
 */
int message(char* message)
{
  return printf("%s%s", message, LINE_END);
}

/**
 Función principal del programa
 */
int main(int argc, char* argv[])
{
  /** Cabeceras HTTP */
  header("Content-Type", "text/html; charset=UTF-8");
  /** Separador entre cabeceras y cuerpo */
  printf("\r\n");
  /** Cuerpo de la respuesta HTTP */
  message("Hello world!");
  /** Regresa un código de salida exitoso */
  return 0;
}
