/**
 Ejemplo de programa simple escrito en C
 */

/** Incluye la biblioteca estándar para entrada-salida */
#include <stdio.h>

/**
 Función principal del programa
 */
int main(int argc, char* argv[])
{
  /** Imprime un mensaje a STDOUT */
  printf("%s\n", "Hello world!");
  /** Regresa un código de salida exitoso */
  return 0;
}
