#!/usr/bin/env python3

import os
import sys
import time

from pymemcache.client.base import Client

EOL = "\r\n"


def cgi_print(message, flush=True):
    print(message, end=EOL, flush=flush)


def banner(message, len=80):
    cgi_print("#" * len)
    cgi_print(f"#	{message}")


def cabeceras():
    cgi_print("Content-Type: text/plain; charset=UTF-8")
    sys.stdout.flush()


def separador():
    cgi_print("")
    sys.stdout.flush()


def cuerpo():
    banner("memcache")

    # client = Client('unix:/run/memcached/memcached.sock')
    # client = Client(("localhost", 11211))
    client = Client(("127.0.0.1", 11211))
    client.flush_all()

    banner("version", 40)

    version = client.version()
    cgi_print(str(version.decode('utf-8')))

    # banner("stats", 40)
    # 
    # stats = client.stats()
    # for element in sorted(stats):
    #     k = element
    #     v = str(stats[element])
    #     cgi_print(f"id: {str(element.decode('utf-8'))}			value: {str(stats[element])}")

    banner("data_dict", 40)

    data_dict = {
        "some_key": "some_value",
    }

    cgi_print(str(data_dict))
    for key in data_dict:
        cgi_print(f"original:	llave: {key}	valor: {data_dict[key]}")
        client.set(key, data_dict[key])
        result = client.get(key)
        cgi_print(f"result:		llave: {key}	valor: {result.decode('utf-8')}")

    banner("data_list", 40)

    data_list = [
        {
            "key": "other_key",
            "value": "other_value",
        }
    ]
    cgi_print(str(data_list))
    for item in data_list:
        cgi_print(f"original:	llave: {item['key']}	valor: {item['value']}")
        client.set(item["key"], item["value"])
        result = client.get(item["key"])
        cgi_print(f"result:		llave: {item['key']}	valor: {result.decode('utf-8')}")

    banner("FIN")

    cgi_print("")
    sys.stdout.flush()


def main():

    cabeceras()
    separador()
    cuerpo()
    time.sleep(1)
    sys.exit(0)


if __name__ == "__main__":
    main()
