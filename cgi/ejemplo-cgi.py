#!/usr/bin/env python3

import os
import sys
import time

EOL = "\r\n"


def procesa():
    if (os.getenv("REQUEST_METHOD") == "GET"):
        cgi_print("GET")
    if (os.getenv("REQUEST_METHOD") == "POST"):
        cgi_print("POST")
        data = {}
        for line in sys.stdin:
            fields = line.split('&')
        for field in fields:
            elements = field.split("=")
            key = elements[0]
            value = elements[1]
            data[key] = value
        cgi_print(str(data))

def cgi_print(message, flush=True):
    print(message, end=EOL, flush=flush)


def banner(message):
    cgi_print("#" * 80)
    cgi_print(f"#	{message}")


def cabeceras():
    cgi_print("Content-Type: text/plain; charset=UTF-8")
    sys.stdout.flush()


def separador():
    cgi_print("")
    sys.stdout.flush()


def cuerpo():
    cgi_print("Hola mundo!")
    cgi_print("")
    sys.stdout.flush()

    banner("Argumentos de línea de comandos")
    for arg in sys.argv:
        cgi_print(f"{arg}")
    
    cgi_print("")
    sys.stdout.flush()
    
    banner("Variables de entorno")
    for var in sorted(os.environ):
        cgi_print(f"{var} = {os.environ[var]}")
    
    banner("STDIN")
    for line in sys.stdin:
        cgi_print(line)
    
    banner("FIN")
    
    cgi_print("")
    sys.stdout.flush()


def main():
    cabeceras()
    separador()
    cuerpo()
    time.sleep(1)
    sys.exit(1)


if __name__ == "__main__":
    main()
