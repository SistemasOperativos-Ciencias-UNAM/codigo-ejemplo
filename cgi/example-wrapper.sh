#!/bin/bash

# Define la cadena con la que terminan las líneas de cabecera y cuerpo de HTTP
# - '\r' es un retorno de línea (ASCII `0x0D`)
# - '\n' es un caracter de nueva línea (ASCII `0x0A`)
LINE_END="\r\n"

# Imprime una cabecera HTTP seguida de la cadena 'LINE_END'
printf "%s: %s${LINE_END}" "Content-Type" "text/html; charset=UTF-8"
# Separador entre cabeceras y cuerpo
printf "${LINE_END}"
# Ejecuta el programa básico y pasa la salida que produce
exec ./example
