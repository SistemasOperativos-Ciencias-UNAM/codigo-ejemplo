#include <stdio.h>

#define EOL "\r\n"

int main(int argc, char* argv[], char* envp[])
{
  printf("%s%s", "Content-Type: text/html; charset=UTF-8", EOL);
  printf(EOL);
  printf("%s%s", "<html><h1>Hola Mundo</h1></html>", EOL);
  return 0;
}
