/*
	http://www.linuxhowtos.org/data/6/U_server.c
	A server in the unix domain
	The pathname of  the socket address is passed as an argument
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#define BUF_SIZE 80

int main(int argc, char *argv[])
{
  int sockfd, newsockfd, servlen, n;
  socklen_t clilen;
  struct sockaddr_un cli_addr, serv_addr;
  char buffer[BUF_SIZE];

  if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
  {
    perror("Creating socket");
    exit(1);
  }
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sun_family = AF_UNIX;
  strcpy(serv_addr.sun_path, argv[1]);
  servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);
  if (bind(sockfd,(struct sockaddr *) &serv_addr, servlen) < 0)
  {
    perror("Binding socket");
    exit(2);
  }

  listen(sockfd, 5);
  clilen = sizeof(cli_addr);
  newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0)
  {
    perror("Accepting");
    exit(3);
  }
  n = read(newsockfd, buffer, BUF_SIZE);
  printf("A connection has been established\n");
  write(1, buffer, n);
  write(newsockfd, "I got your message\n", 19);
  close(newsockfd);
  close(sockfd);
  return 0;
}
