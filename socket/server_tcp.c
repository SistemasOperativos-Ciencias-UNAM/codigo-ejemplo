/*
    C socket server example
	https://www.binarytides.com/server-client-example-c-sockets-linux/
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define BUFFER 2048

int main(int argc, char *argv[])
{
  int socket_desc, client_sock, c, read_size;
  struct sockaddr_in server, client;
  char client_message[BUFFER];
  int port = 0;

  if (argc < 2)
  {
    port = 65535;
  }
  else
  {
    port = atoi(argv[1]);
  }

  // Create socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_desc == -1)
  {
    perror("Error: Could not create socket");
    exit(1);
  }
  puts("Socket created");

  // Prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);

  // Bind
  if (bind(socket_desc,(struct sockaddr *) &server, sizeof(server)) < 0)
  {
    perror("Error: Bind failed");
    exit(2);
  }
  puts("bind done");

  // Listen
  listen(socket_desc, 3);

  while (true)
  {
    // Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);

    // accept connection from an incoming client
    client_sock =
      accept(socket_desc,(struct sockaddr *) &client,(socklen_t *) & c);
    if (client_sock < 0)
    {
      perror("Error: Accept failed");
      exit(3);
    }
    puts("Connection accepted");

    // Receive a message from client
    while ((read_size = recv(client_sock, client_message, BUFFER, 0)) > 0)
    {
      // Send the message back to client
      write(client_sock, client_message, strlen(client_message));
      bzero(client_message, BUFFER);
    }

    if (read_size == 0)
    {
      puts("Client disconnected");
      fflush(stdout);
    }
    else if (read_size == -1)
    {
      perror("Error: recv() failed");
      break;
    }
  }
  return EXIT_SUCCESS;
}
