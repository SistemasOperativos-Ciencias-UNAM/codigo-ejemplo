/*
    C ECHO client example using sockets
	https://www.binarytides.com/server-client-example-c-sockets-linux/
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define MSG_BUFFER 1024
#define SRV_BUFFER 2048

int main(int argc, char *argv[])
{
  int sock;
  struct sockaddr_in server;
  char message[MSG_BUFFER], server_reply[SRV_BUFFER];
  int port = 0;

  if (argc < 2)
  {
    port = 65535;
  }
  else
  {
    port = atoi(argv[1]);
  }

  // Create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1)
  {
    perror("Error: Could not create socket");
    exit(1);
  }
  puts("Socket created");

  server.sin_addr.s_addr = inet_addr("127.0.0.1");
  server.sin_family = AF_INET;
  server.sin_port = htons(port);

  // Connect to remote server
  if (connect(sock,(struct sockaddr *) &server, sizeof(server)) < 0)
  {
    perror("Error: connect failed");
    exit(2);
  }

  puts("Connected");

  // keep communicating with server
  while (true)
  {
    bzero(message, MSG_BUFFER);
    bzero(server_reply, SRV_BUFFER);
    printf("> Enter message : ");
    scanf("%s", message);

    // Send some data
    if (send(sock, message, strlen(message), 0) < 0)
    {
      perror("Error: Send failed");
      exit(2);
    }

    // Receive a reply from the server
    if (recv(sock, server_reply, SRV_BUFFER, 0) < 0)
    {
      perror("Error: recv() failed");
      break;
    }

    puts("< Server reply :");
    puts(server_reply);
  }

  close(sock);
  return EXIT_SUCCESS;
}
