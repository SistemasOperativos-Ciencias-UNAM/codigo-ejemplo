/*
	http://www.linuxhowtos.org/data/6/U_client.c
	A client in the unix domain
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#define BUF_SIZE 80

int main(int argc, char *argv[])
{
  int sockfd, servlen, n;
  struct sockaddr_un serv_addr;
  char buffer[BUF_SIZE];

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sun_family = AF_UNIX;
  strcpy(serv_addr.sun_path, argv[1]);
  servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);
  if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
  {
    perror("Creating socket");
    exit(1);
  }
  if (connect(sockfd,(struct sockaddr *) &serv_addr, servlen) < 0)
  {
    perror("Connecting");
    exit(2);
  }
  printf("Please enter your message: ");
  bzero(buffer, BUF_SIZE);
  fgets(buffer, BUF_SIZE-1, stdin);
  write(sockfd, buffer, strlen(buffer));
  n = read(sockfd, buffer, BUF_SIZE-1);
  printf("The return message was\n");
  write(1, buffer, n);
  close(sockfd);
  return 0;
}
