#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#ifdef __APPLE__
#include <stdlib.h>
#else
#include <error.h>
#endif

int main(void)
{
  DIR * dir;
  char* path = "/nonexistent";
  struct dirent * contents;

  printf("opendir('%s')\n", path);  
  dir = opendir(path);
  if (errno != 0)
  {
    #ifdef _ERROR_H
    error(1, errno, "opendir error");
    #else
    perror("opendir error");
    exit(1);
    #endif
  }

  contents = readdir(dir);
  closedir(dir);
  return 0;
}
