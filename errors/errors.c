#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <err.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef __APPLE__
#include <error.h>
#endif

FILE* open_file(char* filename, char* mode);
int close_file(FILE* file_pointer, char* filename);
int read_data(FILE* file_pointer);
int write_data(char* value, FILE* f);

size_t size = 80;

char* files[] =
{
  "/dev/null",
  "/dev/zero",
  "file",
  "/dev/one",
  NULL,
};

char* modes[] =
{
  "a+",
  "r+",
  "w+",
  NULL,
};

int main(void)
{
  FILE *f;
  int r = 0;
  struct stat buffer;

  for (int i=0 ; files[i]!=NULL ; i++)
  {
    printf("--------\n");
    printf("file: %s\n", files[i]);
    for (int j=0 ; modes[j]!=NULL ; j++)
    {
      printf("\tmode: %s\n", modes[j]);
      r = stat(files[i], &buffer);
      if (r != 0)
      {
        warn("File does not exist: %s\n", files[i]);
      }
      f = open_file(files[i], modes[j]);
      r = read_data(f);
      r = write_data("Hola", f);
      r = close_file(f, files[i]);
    }
  }

  return EXIT_SUCCESS;
}

FILE* open_file(char* filename, char* mode)
{
  FILE *f;
  int err_num = 0;
  char err_msg[size];
  memset(err_msg, 0x00, size);

  f = fopen(filename, mode);
  if (f == NULL)
  {
    err_num = errno;
    sprintf(err_msg, "Cannot open file: %s", filename);
    #ifdef _ERROR_H
    error(EXIT_FAILURE, err_num, err_msg);
    #else
    perror(err_msg);
    exit(EXIT_FAILURE);
    #endif
  }
  return f;
}

int close_file(FILE* file_pointer, char* filename)
{
  int r = 0;
  int err_num = 0;
  char err_msg[size];

  r = fclose(file_pointer);
  if (r != 0)
  {
    err_num = errno;
    sprintf(err_msg, "Cannot close file: %s", filename);
    perror(err_msg);
  }
  return r;
}

int read_data(FILE* file_pointer)
{
  char c = 0x00;
  c = fgetc(file_pointer);
  if (c < 0)
  {
    warnx("fgetc returned EOF");
  }
  return c;
}

int write_data(char* value, FILE* f)
{
  int r = 0;
  r = fputs(value, f);
  if (r < 0)
  {
    warnx("fputs returned EOF");
  }
  return r;
}
