#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(void)
{
  char* path="/nonexistent";
  FILE* file;
  file = fopen(path, "a+");
  if (file == NULL)
  {
    perror("No se pudo abrir el archivo");
    exit(EXIT_FAILURE);
  }
  fclose(file);
  return EXIT_SUCCESS;
}
