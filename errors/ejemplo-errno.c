#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

int main(void)
{
  struct stat buffer;
  char* path = "/dev/nonexistent";
  printf("stat('%s', &buffer);\n", path);
  stat(path, &buffer);
  if (errno != 0)
  {
    fprintf(stderr, "Status: %d, %s\n", errno, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}
