#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <err.h>

int main(void)
{
  size_t BUF_LEN=80;
  char cwd[BUF_LEN];
  char* dir = "/user"; /* Should be "/usr" */
  memset(cwd, '\0', BUF_LEN);

  printf("getcwd()\n");
  if (getcwd(cwd, BUF_LEN) == NULL)
  {
    warn("Could not execute '%s'", "getcwd(3)");
  }

  printf("CWD:\t%s\n",cwd);
  printf("chdir('%s')\n", dir);
  if (chdir(dir) != 0)
  {
    err(1, "Could not execute '%s'", "chdir(3)");
  }
  return 0;
}
