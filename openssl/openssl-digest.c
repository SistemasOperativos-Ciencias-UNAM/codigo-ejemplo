/*
 * Copyright 2019-2021 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * You can obtain a copy in the file LICENSE in the source distribution or at:
 * https://www.openssl.org/source/license.html
 */

/*
 * This demonstration will show how to digest data using
 * a BIO created to read from stdin
 */

/*
 * This is a backported version from the EVP_MD_stdin demo on the
 * OpenSSL 3.1.0-dev master branch [1] using the methods from the EVP_*
 * manpages of OpenSSL version 1.1.1 [2], and the example from the
 * OpenSSL wiki page [3].
 *
 * Author: Andres Hernandez <tonejito@comunidad.unam.mx>
 *
 * [1]: https://github.com/openssl/openssl/blob/master/demos/digest/EVP_MD_stdin.c
 * [2]: https://www.openssl.org/docs/man1.1.1/man3/EVP_Digest.html
 * [3]: https://wiki.openssl.org/index.php/EVP_Message_Digests
 */

#include <stdio.h>
#include <string.h>
#include <openssl/err.h>
#include <openssl/evp.h>

void help(char* name);
int demonstrate_digest(char* algorithm, BIO *input);

int main(int argc, char *argv[])
{
  /* Assign STDIN to OpenSSL input */
  BIO *input = BIO_new_fd(fileno(stdin), 1);

  /* Print the help function if no arguments are present */
  if (argc < 2)
  {
    help(argv[0]);
    exit(1);
  }

  /* Calculate the message digest for 'input' using the specified algorithm */
  if (input != NULL)
  {
    demonstrate_digest(argv[1], input);
    BIO_free(input);
  }

  return 0;
}

void help(char* name)
{
  printf("Usage: %s <digest>\n", name);
  printf("\n");
  printf("\t<digest>: [ md5 | sha1 | sha256 | sha512 | ... ]\n");
  printf("\n");
  printf("Use 'openssl list -digest-algorithms' to get the list of supported digest algorithms\n");
}

int demonstrate_digest(char* algorithm, BIO *input)
{
  int result = 0;
  const EVP_MD *message_digest = NULL;
  EVP_MD_CTX *digest_context = NULL;
  unsigned int digest_length;
  unsigned char *digest_value = NULL;
  unsigned char buffer[512];
  int ii;

  /*
   * Fetch a message digest by name
   * The algorithm name is case insensitive. 
   * See providers(7) for details about algorithm fetching
   */
  message_digest = EVP_get_digestbyname(algorithm);
  if (message_digest == NULL)
  {
    fprintf(stderr, "EVP_MD_fetch could not find %s.\n", algorithm);
    ERR_print_errors_fp(stderr);
    return 0;
  }

  /* Print the selected algorithm */
  fprintf(stderr, "%s\n", algorithm);

  /* Determine the length of the fetched digest type */
  digest_length = EVP_MD_size(message_digest);
  if (digest_length <= 0)
  {
    fprintf(stderr, "EVP_MD_get_size returned invalid size.\n");
    goto cleanup;
  }

  /* Allocate digest_length bytes to store the message digest value */
  digest_value = OPENSSL_malloc(digest_length);
  if (digest_value == NULL)
  {
    fprintf(stderr, "No memory.\n");
    goto cleanup;
  }

  /*
   * Make a message digest context to hold temporary state
   * during digest creation
   */
  digest_context = EVP_MD_CTX_new();
  if (digest_context == NULL)
  {
    fprintf(stderr, "EVP_MD_CTX_new failed.\n");
    ERR_print_errors_fp(stderr);
    goto cleanup;
  }

  /*
   * Initialize the message digest context to use the fetched 
   * digest provider
   */
  if (EVP_DigestInit(digest_context, message_digest) != 1)
  {
    fprintf(stderr, "EVP_DigestInit failed.\n");
    ERR_print_errors_fp(stderr);
    goto cleanup;
  }

  /* Read the input file by chunks and update the message digest*/
  while ((ii = BIO_read(input, buffer, sizeof(buffer))) > 0)
  {
    if (EVP_DigestUpdate(digest_context, buffer, ii) != 1)
    {
      fprintf(stderr, "EVP_DigestUpdate() failed.\n");
      goto cleanup;
    }
  }

  /* Calculate the final message digest  */
  if (EVP_DigestFinal(digest_context, digest_value, &digest_length) != 1)
  {
    fprintf(stderr, "EVP_DigestFinal() failed.\n");
    goto cleanup;
  }
  result = 1;

  /* Print the final message digest */
  for (ii=0; ii<digest_length; ii++)
  {
    fprintf(stdout, "%02x", digest_value[ii]);
  }
  fprintf(stdout, "\n");

  /* Perform cleanup */
cleanup:
  if (result != 1)
  {
    ERR_print_errors_fp(stderr);
  }
  /* OpenSSL free functions will ignore NULL arguments */
  EVP_MD_CTX_free(digest_context);
  OPENSSL_free(digest_value);
  return result;
}
