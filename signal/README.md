# Manejo de señales

En estos programas se pueden ver ejemplos de cómo establecer el manejo de 
señales en C

## Notas

+ Compilar con `make all`
+ Los archivos de código están en la carpeta `src/` y los ejecutables en `bin/`
+ Se puede utilizar `make clean` para eliminar los ejecutables

## Referencias

+ <https://www.gnu.org/software/libc/manual/html_mono/libc.html#toc-Signal-Handling-1>

+ <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Signal-Handling>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Concepts-of-Signals>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Standard-Signals>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Signal-Actions>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Defining-Handlers>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Interrupted-Primitives>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Generating-Signals>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Blocking-Signals>
  - <https://www.gnu.org/software/libc/manual/html_mono/libc.html#Waiting-for-a-Signal>

+ Páginas de manual (RTFM!):

| `En terminal`     | En línea |
|:------------------|:---------|
| `man 7 signal`    | <https://linux.die.net/man/7/signal> |
| `man 2 kill`      | <https://linux.die.net/man/2/kill> |
| `man 2 sigaction` | <https://linux.die.net/man/2/sigaction> |


## [turnoff.us](http://turnoff.us/all/)

![http://turnoff.us/geek/dont-sigkill/][sigkill]

[sigkill]: http://turnoff.us/image/en/dont-sigkill.png

--------

![http://turnoff.us/geek/dont-sigkill-2/][sigkill-2]

[sigkill-2]: http://turnoff.us/image/en/dont-sigkill-2.png

--------

![http://turnoff.us/geek/brothers-conflict/][brothers-conflict]

[brothers-conflict]: http://turnoff.us/image/en/brothers-conflict.png
