# amhello-0.1

Simple hello-world C program with an executable Makefile

This program requires the C compiler and `make` to be installed in the system

You can either execute `make` with the target name or the Makefile directly:

```bash
$ make all
	...

$ ./Makefile all
	...
```
