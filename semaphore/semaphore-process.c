/*
http://www.vishalchovatiya.com/semaphore-between-processes-example-in-c/
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/wait.h>

const char *semName = "vishalchovatiya";

void parent(void);

void child(void);

int main(int argc, char *argv[])
{
    pid_t pid;
    pid = fork();

    if (pid < 0)
    {
        perror("A: fork");
        sleep(1);
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        int status;
        parent();
        wait(&status);
        // waitpid(pid, &status, 0);
        printf("1: Parent	: Done with sem_open\n");
        sleep(1);
    }
    else
    {
        child();
        printf("2: Child	: Done with sem_open\n");
        sleep(1);
    }

    return 0;
}

void parent(void)
{
    sem_t *sem_id = sem_open(semName, O_CREAT, 0600, 0);

    if (sem_id == SEM_FAILED)
    {
        perror("B: Parent	: [sem_open] Failed\n");
        sleep(1);
        return;
    }

    printf("3: Parent	: Wait for Child to Print\n");
    sleep(1);
    if (sem_wait(sem_id) < 0)
    {
        printf("4: Parent	: [sem_wait] Failed\n");
        sleep(1);
    }
    printf("5: Parent	: Child Printed!\n");
    sleep(1);

    if (sem_close(sem_id) != 0)
    {
        perror("C: Parent	: [sem_close] Failed\n");
        sleep(1);
        return;
    }

    if (sem_unlink(semName) < 0)
    {
        printf("6: Parent	: [sem_unlink] Failed\n");
        sleep(1);
        return;
    }
}

void child(void)
{
    sem_t *sem_id = sem_open(semName, O_CREAT, 0600, 0);

    if (sem_id == SEM_FAILED)
    {
        perror("D: Child	: [sem_open] Failed\n");
        sleep(1);
        return;
    }

    printf("7: Child	: I am done! Release Semaphore\n");
    sleep(1);
    if (sem_post(sem_id) < 0)
    {
        printf("8: Child	: [sem_post] Failed\n");
        sleep(1);
    }
}
