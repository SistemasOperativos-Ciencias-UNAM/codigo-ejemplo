#!/usr/bin/env bash

# http://awk.freeshell.org/PrintASingleQuote
# https://stackoverflow.com/a/33680131

set -euo pipefail

MEMCACHED_HOST=localhost
MEMCACHED_PORT=11211

until nc -z ${MEMCACHED_HOST} ${MEMCACHED_PORT} 2>/dev/null
do
  echo "ERROR" >&2
  exit 1
done

SERVER_STATS=$(
  printf "stats\r\nquit\r\n" | \
  nc ${MEMCACHED_HOST} ${MEMCACHED_PORT} | \
  tr -d '\r' | \
  head
) 2>/dev/null

SERVER_STATS_JSON=$(
  echo "${SERVER_STATS}" | \
  awk -v q='"' -v c=',' '{if($1=="STAT"){print "  " q $2 q ": " q $3 q c}}' | \
  sed -e '$ s/.$//'
) 2>/dev/null

SERVER_STATS_JSON=$(printf "{\n%s\n}\n" "${SERVER_STATS_JSON}") 2>/dev/null

echo "${SERVER_STATS_JSON}"
