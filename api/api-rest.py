#!/usr/bin/env python3

import os, json, yaml

from pathlib import Path
from flask import Flask, Response, jsonify
from flask_cors import CORS, cross_origin
from apiflask import APIFlask
from apiflask.fields import Integer, String
from apiflask.validators import Length, OneOf
from flask_swagger_ui import get_swaggerui_blueprint
from marshmallow import Schema, fields

API_HOST = "localhost"
# API_HOST = "127.0.0.1"
# API_PORT = 80
API_PORT = 9999

app = APIFlask(__name__, spec_path="/openapi.json")

cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config["CORS_HEADERS"] = "Content-Type"

app.config["SPEC_FORMAT"] = "json"
app.config["SYNC_LOCAL_SPEC"] = True
app.config["LOCAL_SPEC_PATH"] = Path(app.root_path) / "openapi.json"
app.config["LOCAL_SPEC_PATH"] = os.path.join(app.root_path, "openapi.json")
app.config["INFO"] = {
    "description": "...",
    "termsOfService": "http://example.com",
    "contact": {
        "name": "API Support",
        "url": "http://www.example.com/support",
        "email": "support@example.com",
    },
    "license": {
        "name": "Apache 2.0",
        "url": "http://www.apache.org/licenses/LICENSE-2.0.html",
    },
}
# openapi.tags
app.config["TAGS"] = [
    {"name": "Dia", "description": "The description of the **Dia** tag."},
]
# openapi.servers
app.config["SERVERS"] = [
    {"name": "Development Server", "url": f"http://{API_HOST}:{API_PORT}"},
    {"name": "Production Server", "url": f"http://{API_HOST}:{API_PORT}"},
    {"name": "Testing Server", "url": f"http://{API_HOST}:{API_PORT}"},
]
# openapi.externalDocs
app.config["EXTERNAL_DOCS"] = {
    "description": "Find more info here",
    "url": f"http://{API_HOST}:{API_PORT}/docs",
}


SWAGGER_URL = "/docs"  # URL for exposing Swagger UI (without trailing '/')
API_URL = f"http://{API_HOST}:{API_PORT}/swagger.json"  # Our API url (can of course be a local resource)

# Call factory function to create our blueprint
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={"app_name": "Test application"},  # Swagger UI config overrides
)

app.register_blueprint(swaggerui_blueprint)


tabla = {
    "tabla": {
        "1999-12-31": {
            "max": 100,
            "valores": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
        },
        "2000-01-01": {
            "max": 111,
            "valores": [11, 22, 33, 44, 55, 66, 77, 88, 99, 111],
        },
    }
}

version = {
    "status": {
        "version": "0.0.1",
        "semestre": "2025-1",
        "equipo": "Equipo-ABCD-EFGH-IJKL-MNOP",
        "integrantes": {
            "123456789": "Apellido Apellido Nombre Nombre",
            "234567890": "Apellido Apellido Nombre Nombre",
            "345678901": "Apellido Apellido Nombre Nombre",
            "456789012": "Apellido Apellido Nombre Nombre",
        },
    }
}

estado = {
    "backend": {
        "host": "127.0.0.1",
        "port": 11211,
        "socket": "/run/memcached.sock",
        "version": "1.6.32",
        "stats": {
            "pid": "16384",
            "uptime": "65535",
            "time": "1712600241",
            "version": "1.6.32",
            "libevent": "2.1.12-stable",
            "pointer_size": "64",
            "rusage_user": "0.654321",
            "rusage_system": "0.123456",
            "max_connections": "1024",
            "curr_connections": "2",
        },
    }
}

delete = {"status": "DELETED"}

dia_get = {
    "dia": {
        "fecha": "1999-12-31",
        "categoria": "Regular",
        "max": 100,
        "valores": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
    }
}

dia_post = {
  "dia": {
    "fecha": "1999-12-31",
    "valores": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
  }
}

flush = {"status": "FLUSH"}


class DiaIn(Schema):
    fecha = String(
        required=True,
        validate=Length(0, 10),
        metadata={"title": "Fecha", "description": "Llave: YYYY-MM-DD"},
    )
    valores = fields.List(fields.Integer())


class DiaOut(Schema):
    fecha = String(metadata={"title": "Pet ID", "description": "The ID of the pet."})
    maximo = fields.Integer()
    valores = fields.List(fields.Integer())


def main():
    app.run(host=API_HOST, port=API_PORT, debug=True)


@app.route("/", methods=["GET"])
@app.get("/")
@app.doc(tags=["_"])
def get_raiz():
    return jsonify({})


@app.route("/version", methods=["GET"])
@app.get("/version")
@app.doc(tags=["Version"])
def get_version():
    data = json.dumps(version, indent=2) + "\n"
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


@app.route("/estado", methods=["GET"])
@app.get("/estado")
@app.doc(tags=["Estado"])
def get_estado():
    data = json.dumps(estado, indent=2) + "\n"
    # data = yaml.dump(estado, indent=2, explicit_start=True)
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    # response.mimetype = "application/x-yaml"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


@app.route("/dia/<string:dia_id>", methods=["GET"])
@app.get("/dia/<string:dia_id>")
@app.output(DiaOut, status_code=200, description="Output data of a dia")
@app.doc(tags=["Dia"])
def get_dia(dia_id):
    data = json.dumps(dia_get, indent=2) + "\n"
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


@app.route("/dia", methods=["POST", "PUT", "PATCH"])
@app.post("/dia")
@app.put("/dia")
@app.patch("/dia")
@app.input(DiaIn)
@app.doc(tags=["Dia"])
def create_dia(json_data):
    data = json.dumps(dia_post, indent=2) + "\n"
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


@app.route("/dia/<string:dia_id>", methods=["DELETE"])
@app.delete("/dia/<string:dia_id>")
@app.output({}, status_code=204, description="Empty")
@app.doc(tags=["Dia"])
def delete_dia(dia_id):
    data = json.dumps(delete, indent=2) + "\n"
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


@app.route("/tabla", methods=["GET"])
@app.get("/tabla")
@app.doc(tags=["Tabla"])
def get_tabla():
    global tabla
    data = json.dumps(tabla, indent=2) + "\n"
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


@app.route("/tabla", methods=["DELETE"])
@app.delete("/tabla")
@app.doc(tags=["Tabla"])
def delete_tabla():
    global tabla
    tabla = {"tabla": {}}
    data = json.dumps(flush, indent=2) + "\n"
    response = Response()
    response.data = data
    response.mimetype = "application/json"
    response.charset = "UTF-8"
    response.headers["Content-Disposition"] = "inline"
    return response


if __name__ == "__main__":
    main()
