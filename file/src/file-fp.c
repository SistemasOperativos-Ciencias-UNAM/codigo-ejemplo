#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>

/* Nombre de archivo */
char *archivo = "archivo.txt";

int main(void)
{
  /* Apuntador a archivo */
  FILE* fp = NULL;

  /* Abriendo apuntador a archivo */
  fp = fopen(archivo, "w+");
  /* Listando las propiedades del apuntador a archivo */
  printf("Las propiedades del apuntador a archivo son:\n");
  printf("\t_fileno:\t%d\n", fp->_fileno);
  printf("\t_flags:\t%#x\n", fp->_flags);
  printf("\t_flags2:\t%#x\n", fp->_flags2);

  /* Cerrando apuntador a archivo */
  fclose(fp);

  return EXIT_SUCCESS;
}
