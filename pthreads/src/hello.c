/******************************************************************************
* FILE: hello.c
* DESCRIPTION:
*   A "hello world" Pthreads program.  Demonstrates thread creation and
*   termination.
* AUTHOR: Blaise Barney
* REVISION: 08/09/11
* LAST REVISED: 23/08/17  Andres Hernandez
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NUM_THREADS	5

void *PrintHello(void *threadid);

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    for(t=0; t<NUM_THREADS; t++)
    {
        printf("In main: creating thread %ld\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *)t);
        if (rc)
        {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    printf("In main: about to exit\n");
    /* Last thing that main() should do */
    pthread_exit(NULL);
}

void *PrintHello(void *threadid)
{
    long tid;
#ifdef SLEEP
    sleep(1);
#endif
    tid = (long) threadid;
    printf("Hello World! It's me, thread #%ld!\n", tid);
    pthread_exit(NULL);
}
