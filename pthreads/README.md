# Práctica: Programación con hilos

En esta práctica se verán los métodos aplicados para el uso de las funciones 
de la biblioteca pthread que ayudan para la implementación de hilos en C

## Notas

+ Para descargar el código fuente original ejecutar `make get`
+ Sera necesario modificar manualmente los archivos fuente para agregar las 
  bibliotecas faltantes en las directivas del preprocesador
+ Este repositorio cuenta con los archivos ya modificados, se pueden 
  compilar con `make all`

## Referencias

> POSIX Threads Programming <br/>
> Blaise Barney <br/>
> Lawrence Livermore National Laboratory

+ <https://computing.llnl.gov/tutorials/>
+ <https://computing.llnl.gov/tutorials/pthreads/#Overview>
+ <https://computing.llnl.gov/tutorials/pthreads/exercise.html#Exercise1>
