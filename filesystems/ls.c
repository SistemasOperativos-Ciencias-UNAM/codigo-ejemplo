#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

int main(int argc, char* argv[])
{
  /* Estructura con el contenido del directorio */
  struct dirent **namelist;
  /* Contadores */
  int i=0,n=0;
  /* Ruta que se utilizará */
  char* path="/";
  /* Asigna una ruta desde la línea de comandos */
  if (argc > 1)
  {
    path = argv[1];
  }

  /* Lista el directorio y devuelve el número de elementos */
  n = scandir(path, &namelist, NULL, alphasort);
  /* Imprime una cabecera con el nombre del directorio y el número de elementos */
  printf("directory: '%s'\titems: %d\n", path, n);
  /* Revisa el número de elementos */
  if (n < 0)
  {
    /* Imprime un mensaje de error y el mensaje asociado a 'errno' */
    perror("scandir");
  }
  else
  {
    /* Imprime cada elemento en la lista de archivos contenidos en el directorio */
    while (i<n)
    {
      /* Imprime los campos de la estructura dirent para cada elemento */
      printf("%llu\t%s\n", namelist[i]->d_ino, namelist[i]->d_name);
      /* Libera esta entrada de la estructura */
      free(namelist[i]);
      i++;
    }
    /* Libera la lista de estructuras */
    free(namelist);
  }
}
