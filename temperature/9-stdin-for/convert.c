/* Convierte entre grados Celcius, Kelvin, Fahrenheit y Rankine */
#include <stdio.h>
#include <stdlib.h>
#include "logic.h"

int main(int argc, char* argv[])
{
  int   i = 0;
  int   j = 0;
  float A = 0;
  float B = 0;
  char U1 = 0;
  // char U2 = 0;
  float values[4] = {0,0,0,0};

  printf("Convierte entre grados Celcius, Kelvin, Fahrenheit y Rankine");
  printf("\n");

  /* El nombre del programa es el primer argumento de la línea de comando: argv[0] */
  /* Los demás argumentos de línea de comandos se acceden desde argv[1] */
  if (argc == 2)
  {
    /* Lee la primer letra del primer y segundo argumento para identificar las unidades */
    U1 = argv[1][0];
    // U2 = argv[2][0];
  }
  else
  {
    perror("ERROR: Argumentos de línea de comandos");
  }

  for (i=0 ; i<100 ; i++)
  {
    /* Sale del ciclo si ya no puede leer más elementos desde STDIN */
    if (scanf(" %f", &A) == EOF)
      break;

    convert(values, A, U1);
    printf("%d:\t%f C\t%f K\t%f F\t%f R\n", i, values[0], values[1], values[2], values[3]);
  }

  return 0;
}
