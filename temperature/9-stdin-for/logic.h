// #define LOGIC_H
// #ifndef LOGIC_H

#include "logic.c"

void error(void);

float C_to_K(float C);
float C_to_F(float C);
float C_to_R(float C);

float F_to_C(float F);
float F_to_K(float F);
float F_to_R(float F);

float K_to_C(float K);
float K_to_F(float K);
float K_to_R(float K);

float R_to_C(float R);
float R_to_K(float R);
float R_to_F(float R);

/*
float* convert_C(float C);
float* convert_K(float K);
float* convert_F(float F);
float* convert_R(float R);
*/

// float* convert(float A, char U1);
void convert(float values[], float A, char U1);

// #endif
