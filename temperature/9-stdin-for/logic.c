#define     OFFSET_K   273.15
const int   OFFSET_F = 32;
const float OFFSET_R = 459.67;

void error(void)
{
  printf("ERROR: No se reconoce la operación\n");
  exit(1);
}


float C_to_F(float C)
{
  return C * (9.0/5.0) + OFFSET_F;
}

float C_to_K(float C)
{
  return C + OFFSET_K;
}

float C_to_R(float C)
{
  return (C + OFFSET_K) * (9.0/5.0);
}


float F_to_C(float F)
{
  return (F - OFFSET_F) * (5.0/9.0);
}

float F_to_K(float F)
{
  return (F + OFFSET_R) * (5.0/9.0);
}

float F_to_R(float F)
{
  return F + OFFSET_R;
}


float K_to_C(float K)
{
  return K - OFFSET_K;
}

float K_to_F(float K)
{
  return K * (9.0/5.0) - OFFSET_R;
}

float K_to_R(float K)
{
  return K * (9.0/5.0);
}


float R_to_C(float R)
{
  return R * (5.0/9.0) - OFFSET_K;
}

float R_to_K(float R)
{
  return R * (5.0/9.0);
}

float R_to_F(float R)
{
  return R - OFFSET_R;
}

/*
float* convert_C(float C)
{
  float values[4] = {0,0,0,0};
  values[0] = C;
  values[1] = C_to_K(C);
  values[2] = C_to_F(C);
  values[3] = C_to_R(C);
  return values;
}

float* convert_K(float K)
{
  float values[4] = {0,0,0,0};
  values[0] = K_to_C(K);
  values[1] = K;
  values[2] = C_to_F(K);
  values[3] = C_to_R(K);
  return values;
}

float* convert_F(float F)
{
  float values[4] = {0,0,0,0};
  values[0] = F_to_C(F);
  values[1] = F_to_K(F);
  values[2] = F;
  values[3] = F_to_R(F);
  return values;
}

float* convert_R(float R)
{
  float values[4] = {0,0,0,0};
  values[0] = R_to_C(R);
  values[1] = R_to_K(R);
  values[2] = R_to_F(R);
  values[3] = R;
  return values;
}
*/

// float* convert(float A, char U1)
void convert(float values[], float A, char U1)
{
  /* Identifica la operación a realizar */
  /* Se llama a la función correspondiente para procesar el resultado */
  float B = 0;
  // float values[4] = {0,0,0,0};

  // TODO: Mover enum a este programa para utilizarlo en lugar de los índices numéricos
  switch (U1)
  {
    case 'C':
      // values = convert_C(A);
      values[0] = A;
      values[1] = C_to_K(A);
      values[2] = C_to_F(A);
      values[3] = C_to_R(A);
      break;
    case 'K':
      // values = convert_K(A);
      values[0] = K_to_C(A);
      values[1] = A;
      values[2] = C_to_F(A);
      values[3] = C_to_R(A);
      break;
    case 'F':
      // values = convert_F(A);
      values[0] = F_to_C(A);
      values[1] = F_to_K(A);
      values[2] = A;
      values[3] = F_to_R(A);
      break;
    case 'R':
      // values = convert_R(A);
      values[0] = R_to_C(A);
      values[1] = R_to_K(A);
      values[2] = R_to_F(A);
      values[3] = A;
      break;
    default:
      error();
  }
  // return values;
}
