/* Convierte entre grados Fahrenheit y Celcius */
#include <stdio.h>

float F_to_C(float F)
{
  /* El valor 5/9 se debe convertir a "float" para que la operación sea exitosa */
  return (F - 32) * (5.0/9.0);
}

float C_to_F(float C)
{
  /* El valor 9/5 se debe convertir a "float" para que la operación sea exitosa */
  return C * (9.0/5.0) + 32;
}

float convert(float A, char U)
{
  /* Identifica la operación a realizar */
  /* Se llama a la función correspondiente para procesar el resultado */
  float B = 0;

  if (U == 'F')
  {
    printf("Convirtiendo grados Fahrenheit a Celcius\n");
    B = F_to_C(A);
    printf("El equivalente de grados Celcius es:\t%f\n", B);
  }
  else
  {
    if (U == 'C')
    {
      printf("Convirtiendo grados Celcius a Fahrenheit\n");
      B = C_to_F(A);
      printf("El equivalente de grados Fahrenheit es:\t%f\n", B);
    }
    else
    {
      printf("ERROR: No se reconoce la operación\n");
    }
  }
  return B;
}

int main(void)
{
  float A = 0;
  char U = 0;
  printf("Convierte entre grados Fahrenheit y Celcius");
  printf("\n");
  /* Lee los valores de entrada */
  printf("Introduce el valor de grados:\t");
  scanf("%f", &A);
  /* El "espacio en blanco" antes de %c indica que se deben descartar */
  /* los caracteres de espacio o enter antes de convertir a 'char'*/
  printf("Introduce la unidad (C o F):\t");
  scanf(" %c", &U);
  convert(A, U);
  return 0;
}
