/* Convierte entre grados Fahrenheit, Celcius y Kelvin */
#include <stdio.h>
#include <stdlib.h>
#include "logic.h"

int main(int argc, char* argv[])
{
  float A = 0;
  char U1 = 0;
  char U2 = 0;
  printf("Convierte entre grados Fahrenheit, Celcius y Kelvin");
  printf("\n");

  /* El nombre del programa es el primer argumento de la línea de comando: argv[0] */
  /* Los demás argumentos de línea de comandos se acceden desde argv[1] */
  if (argc == 4)
  {
    /* Convierte el argumento de línea de comandos de cadena a float */
    A = atof(argv[1]);
    /* Lee la primer letra del segundo y tercer argumento para identificar las unidades */
    U1 = argv[2][0];
    U2 = argv[3][0];
  }
  else
  {
    perror("ERROR: Argumentos de línea de comandos");
  }

  convert(A, U1, U2);
  return 0;
}
