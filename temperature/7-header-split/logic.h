// #define LOGIC_H
// #ifndef LOGIC_H

#include "logic.c"

void error(void);

float F_to_C(float F);

float C_to_F(float C);

float C_to_K(float C);

float K_to_C(float K);

float F_to_K(float F);

float K_to_F(float K);

float convert(float A, char U1, char U2);

// #endif
