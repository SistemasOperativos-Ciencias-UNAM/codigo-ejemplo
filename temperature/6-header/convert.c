/* Convierte entre grados Fahrenheit, Celcius y Kelvin */
#include <stdio.h>
#include <stdlib.h>
#include "logic.h"

int main(void)
{
  float A = 0;
  char U1 = 0;
  char U2 = 0;
  printf("Convierte entre grados Fahrenheit, Celcius y Kelvin");
  printf("\n");
  /* Lee los valores de entrada */
  printf("Introduce el valor de grados:\t");
  scanf("%f", &A);
  /* El "espacio en blanco" antes de %c indica que se deben descartar */
  /* los caracteres de espacio o enter antes de convertir a 'char'*/
  printf("Introduce la unidad de origen	(C , F o K):\t");
  scanf(" %c", &U1);
  printf("Introduce la unidad de destino	(C , F o K):\t");
  scanf(" %c", &U2);
  convert(A, U1, U2);
  return 0;
}
