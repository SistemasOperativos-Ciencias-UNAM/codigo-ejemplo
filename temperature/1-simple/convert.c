/* Convierte grados Fahrenheit a Celcius */
#include <stdio.h>

int main(void)
{
  float F = 0;
  float C = 0;
  printf("Convierte grados Fahrenheit a Celcius");
  printf("\n");
  /* Lee el valor de entrada */
  printf("Introduce el valor en grados Fahrenheit:\t");
  scanf("%f", &F);
  /* El valor 5/9 se debe convertir a "float" para que la operación sea exitosa */
  C = (F - 32) * (5.0/9.0);
  printf("El equivalente de grados Celcius es:\t%f\n", C);
  return 0;
}
