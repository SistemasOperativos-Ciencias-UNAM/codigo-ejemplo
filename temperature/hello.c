/* Directiva include del preprocesador */
#include <stdio.h>

/* Definición de la función principal del programa */
int main(void)
{
  /* Imprime la cadena "Hello world" seguida de un retorno de línea */
  printf("Hello world\n");
  /* Regresa el código de salida 0 (éxito) */
  return 0;
}
