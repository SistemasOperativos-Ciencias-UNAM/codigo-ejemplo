/* Convierte entre grados Fahrenheit, Celcius y Kelvin */
#include <stdio.h>
#include <stdlib.h>
#include "logic.h"

int main(int argc, char* argv[])
{
  int   i = 0;
  float A = 0;
  // float B = 0;
  char U1 = 0;
  // char U2 = 0;
  // float values[4] = {0,0,0,0};
  temperature temp;
  char* input = NULL;
  char* output = NULL;
  FILE* input_file = NULL;
  FILE* output_file = NULL;

  printf("Convierte entre grados Fahrenheit, Celcius y Kelvin");
  printf("\n");

  /* El nombre del programa es el primer argumento de la línea de comando: argv[0] */
  /* Los demás argumentos de línea de comandos se acceden desde argv[1] */
  if (argc == 4)
  {
    /* Lee la primer letra del primer y segundo argumento para identificar las unidades */
    U1 = argv[1][0];
    // U2 = argv[2][0];
    /* Obtiene el nombre del archivo de entrada del argumento de línea de comandos*/
    input = argv[2];
    output = argv[3];
  }
  else
  {
    perror("ERROR: Argumentos de línea de comandos");
  }

  input_file = fopen(input, "r");
  if (input_file == NULL)
  {
    perror("ERROR: No se puede abrir el archivo de entrada\n");
    exit(-1);
  }

  output_file = fopen(output, "w+");
  if (input_file == NULL)
  {
    perror("ERROR: No se puede abrir el archivo de salida\n");
    exit(-1);
  }

  while(fscanf(input_file, " %f", &A) != EOF)
  {
    i++;
    convert(&temp, A, U1);
    fprintf(output_file, "%d:\t%f C\t%f K\t%f F\t%f R\n", i, temp.C, temp.K, temp.F, temp.R);
  }
  fflush(NULL);
  fclose(input_file);
  fclose(output_file);
  return 0;
}
