// #define LOGIC_H
// #ifndef LOGIC_H

#include "logic.c"

void error(void);

float C_to_K(float C);
float C_to_F(float C);
float C_to_R(float C);

float F_to_C(float F);
float F_to_K(float F);
float F_to_R(float F);

float K_to_C(float K);
float K_to_F(float K);
float K_to_R(float K);

float R_to_C(float R);
float R_to_K(float R);
float R_to_F(float R);

void convert(temperature *temp, float A, char U1);

// #endif
