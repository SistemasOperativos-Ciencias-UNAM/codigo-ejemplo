#define     OFFSET_K   273.15
const int   OFFSET_F = 32;
const float OFFSET_R = 459.67;

enum unit { C, K, F, R };

typedef struct temperature_struct
{
  float C;
  float K;
  float F;
  float R;
} temperature;

void error(void)
{
  printf("ERROR: No se reconoce la operación\n");
  exit(1);
}


float C_to_F(float C)
{
  return C * (9.0/5.0) + OFFSET_F;
}

float C_to_K(float C)
{
  return C + OFFSET_K;
}

float C_to_R(float C)
{
  return (C + OFFSET_K) * (9.0/5.0);
}


float F_to_C(float F)
{
  return (F - OFFSET_F) * (5.0/9.0);
}

float F_to_K(float F)
{
  return (F + OFFSET_R) * (5.0/9.0);
}

float F_to_R(float F)
{
  return F + OFFSET_R;
}


float K_to_C(float K)
{
  return K - OFFSET_K;
}

float K_to_F(float K)
{
  return K * (9.0/5.0) - OFFSET_R;
}

float K_to_R(float K)
{
  return K * (9.0/5.0);
}


float R_to_C(float R)
{
  return R * (5.0/9.0) - OFFSET_K;
}

float R_to_K(float R)
{
  return R * (5.0/9.0);
}

float R_to_F(float R)
{
  return R - OFFSET_R;
}

void convert(temperature *temp, float A, char U1)
{
  /* Identifica la operación a realizar */
  /* Se llama a la función correspondiente para procesar el resultado */

  // TODO: Implementar manejo de errores para rechazar valores de temperatura menores que el "cero absoluto"
  switch (U1)
  {
    case 'C':
      temp->C = A;
      temp->K = C_to_K(A);
      temp->F = C_to_F(A);
      temp->R = C_to_R(A);
      break;
    case 'K':
      temp->C = K_to_C(A);
      temp->K = A;
      temp->F = C_to_F(A);
      temp->R = C_to_R(A);
      break;
    case 'F':
      temp->C = F_to_C(A);
      temp->K = F_to_K(A);
      temp->F = A;
      temp->R = F_to_R(A);
      break;
    case 'R':
      temp->C = R_to_C(A);
      temp->K = R_to_K(A);
      temp->F = R_to_F(A);
      temp->R = A;
      break;
    default:
      error();
  }
}
