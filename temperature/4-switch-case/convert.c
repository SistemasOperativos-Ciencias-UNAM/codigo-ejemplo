/* Convierte entre grados Fahrenheit, Celcius y Kelvin */
#include <stdio.h>
#include <stdlib.h>

#define     KELVIN     273.15
const int   OFFSET_C = 32;
const float OFFSET_K = 459.67;

void error(void)
{
  printf("ERROR: No se reconoce la operación\n");
  exit(1);
}

float F_to_C(float F)
{
  printf("Convirtiendo grados Fahrenheit a Celcius\n");
  /* El valor 5/9 se debe convertir a "float" para que la operación sea exitosa */
  return (F - OFFSET_C) * (5.0/9.0);
}

float C_to_F(float C)
{
  printf("Convirtiendo grados Celcius a Fahrenheit\n");
  /* El valor 9/5 se debe convertir a "float" para que la operación sea exitosa */
  return C * (9.0/5.0) + OFFSET_C;
}

float C_to_K(float C)
{
  printf("Convirtiendo grados Celcius a Kelvin\n");
  return (C + KELVIN);
}

float K_to_C(float K)
{
  printf("Convirtiendo grados Kelvin a Celcius\n");
  return (K - KELVIN);
}

float F_to_K(float F)
{
  printf("Convirtiendo grados Fahrenheit a Kelvin\n");
  /* El valor 5/9 se debe convertir a "float" para que la operación sea exitosa */
  return (F + OFFSET_K) * (5.0/9.0);
}

float K_to_F(float K)
{
  printf("Convirtiendo grados Kelvin a Fahrenheit\n");
  /* El valor 9/5 se debe convertir a "float" para que la operación sea exitosa */
  return K * (9.0/5.0) - OFFSET_K;
}

float convert(float A, char U1, char U2)
{
  /* Identifica la operación a realizar */
  /* Se llama a la función correspondiente para procesar el resultado */
  float B = 0;

  switch (U1)
  {
    case 'F':
      if (U2 == 'C')
      {
        B = F_to_C(A);
      }
      else
      {
        if (U2 == 'K')
        {
          B = F_to_K(A);
        }
        else
        {
          error();
        }
      }
      break;
    case 'C':
      if (U2 == 'F')
        B = C_to_F(A);
      else
      {
        if (U2 == 'K')
          B = C_to_K(A);
        else
          error();
      }
      break;
    case 'K':
      switch (U2)
      {
        case 'F':
          B = K_to_F(A);
          break;
        case 'C':
          B = K_to_C(A);
          break;
        default:
          error();
      }
      break;
    default:
      error();
  }
  printf("%f\n", B);
  return B;
}

int main(void)
{
  float A = 0;
  char U1 = 0;
  char U2 = 0;
  printf("Convierte entre grados Fahrenheit, Celcius y Kelvin");
  printf("\n");
  /* Lee los valores de entrada */
  printf("Introduce el valor de grados:\t");
  scanf("%f", &A);
  /* El "espacio en blanco" antes de %c indica que se deben descartar */
  /* los caracteres de espacio o enter antes de convertir a 'char'*/
  printf("Introduce la unidad de origen	(C , F o K):\t");
  scanf(" %c", &U1);
  printf("Introduce la unidad de destino	(C , F o K):\t");
  scanf(" %c", &U2);
  convert(A, U1, U2);
  return 0;
}
