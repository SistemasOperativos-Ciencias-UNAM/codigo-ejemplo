#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "mjson.h"

/* datos de entrada */

enum procedencia {
    P_LOCAL    = 'L',
    P_FORANEO  = 'F',
    P_INVALIDO = '\0',
};

enum holograma {
    H_EXENTO   = 'E',
    H_CERO     = '0',
    H_UNO      = '1',
    H_DOS      = '2',
    H_NA       = 'X',
    H_INVALIDO = '\0',
};

/* data struct */
struct datos_t {
    char fecha[11];
    char placas[8];
    char procedencia;
    char holograma;
    bool contingencia;
};

/* datos de salida */

enum dia_semana {
    D_LUNES     =  1,
    D_MARTES    =  2,
    D_MIERCOLES =  3,
    D_JUEVES    =  4,
    D_VIERNES   =  5,
    D_SABADO    =  6,
    D_DOMINGO   =  7,
    D_INVALIDO  = -1,
};

enum engomado {
    E_AMARILLO =  5,
    E_ROSA     =  7,
    E_ROJO     =  3,
    E_VERDE    =  1,
    E_AZUL     =  9,
    E_INVALIDO = -1,
};

struct salida_t {
    char fecha[11];
    enum dia_semana dia;
    char placas[8];
    char procedencia;
    char holograma;
    enum engomado engomado;
    bool circula;
};

/* Parse function to associate JSON fields to struct members */
int json_datos_read(const char *buffer, struct datos_t *datos)
{
    /* Initialize data struct members */
    bzero(datos->fecha, sizeof(datos->fecha));
    bzero(datos->placas, sizeof(datos->placas));
    datos->procedencia = P_INVALIDO;
    datos->holograma = H_INVALIDO;
    datos->contingencia = 0;

    /* Mapping of JSON attributes to struct members */
    const struct json_attr_t json_attrs[] = {
        {
            "fecha",
            t_string,
            .addr.string = datos->fecha,
            .len = sizeof(datos->fecha)
        },
        {
            "placas",
            t_string,
            .addr.string = datos->placas,
            .len = sizeof(datos->placas)
        },
        {
            "procedencia",
            t_character,
            .addr.character = &datos->procedencia,
            .dflt.character = P_INVALIDO
        },
        {
            "holograma",
            t_character,
            .addr.character = &datos->holograma,
            .dflt.character = H_INVALIDO
        },
        {
            "contingencia",
            t_boolean,
            .addr.boolean = &datos->contingencia,
            .dflt.boolean = 0
        },
        /* Terminator */
        {NULL},
    };

    /* Parse the JSON string from buffer */
    return json_read_object(buffer, json_attrs, NULL);
}

void salida_json(struct datos_t *datos)
{
    struct salida_t *salida = malloc(sizeof(struct salida_t));
    
    /* Initialize data struct members */
    /*
    bzero(salida->fecha, sizeof(salida->fecha));
    salida->dia = D_INVALIDO;
    bzero(salida->placas, sizeof(salida->placas));
    salida->procedencia = P_INVALIDO;
    salida->holograma = H_INVALIDO;
    salida->engomado = E_INVALIDO;
    salida->circula = 0;
    */

    strncpy(salida->fecha, datos->fecha, sizeof(salida->fecha));
    salida->dia = D_LUNES;
    strncpy(salida->placas, datos->placas, sizeof(salida->placas));
    salida->procedencia = datos->procedencia;
    salida->holograma = datos->holograma;
    salida->engomado = E_AMARILLO;
    salida->circula = true;
    
    printf("\n\t%s\n\n", "Datos de salida");
    printf("fecha:\t\t%s\n", salida->fecha);
    printf("dia:\t\t%d\n", salida->dia);
    printf("placas:\t\t%s\n", salida->placas);
    printf("procedencia:\t%c\n", salida->procedencia);
    printf("holograma:\t%c\n", salida->holograma);
    printf("engomado:\t%d\n", salida->engomado);
    printf("circula:\t%d\n", salida->circula);
    printf("circula:\t%s\n", salida->circula ? "true" : "false");
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: ./json_demo_simple <input JSON string>\n");
        return -1;
    }
    /* Allocate space for datos_t */
    struct datos_t *datos = malloc(sizeof(struct datos_t));

    /* Call datos_t parsing function */
    int status = json_datos_read(argv[1], datos);

    if (status == 0) {
        printf("\n\t%s\n\n", "Datos de entrada");
        printf("fecha:\t\t%s\n", datos->fecha);
        printf("placas:\t\t%s\n", datos->placas);
        printf("procedencia:\t%c\n", datos->procedencia);
        printf("holograma:\t%c\n", datos->holograma);
        printf("contingencia:\t%d\n", datos->contingencia);
        printf("contingencia:\t%s\n", datos->contingencia ? "true" : "false");

        if (datos->procedencia == P_LOCAL)
            printf("procedencia local\n");
        if (datos->holograma == H_EXENTO)
            printf("holograma exento\n");
            
        /* magia() */

        salida_json(datos);
    }
    else {
        puts(json_error_string(status));
    }

    return status;
}
