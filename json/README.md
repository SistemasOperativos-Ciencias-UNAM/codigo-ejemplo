# Ejemplos de `microjson`

Estos archivos de código fuente utilizan la biblioteca `microjson` escrita por Eric S Raymond.
Descargar los archivos del código fuente versión 1.7 de la biblioteca ([`mjson.h`][mjson-h] y [`mjson.c`][mjson-c]) del repositorio oficial y ponerlos en esta carpeta.

- <https://gitlab.com/esr/microjson/-/tree/1.7>

[mjson-h]: https://gitlab.com/esr/microjson/-/raw/1.7/mjson.h?ref_type=tags
[mjson-c]: https://gitlab.com/esr/microjson/-/raw/1.7/mjson.c?ref_type=tags&inline=false

Compilar con `make`, se obtendrán 3 binarios (`api-input-*`) que corresponden a los archivos de código fuente en C.

El script de pruebas se llama `test.sh`, puede invocarse diréctamente o utilizando `make test`.

## Referencias

- <https://gitlab.com/esr/microjson.git>
- <https://gitlab.com/esr/microjson/-/blob/1.7/microjson.adoc>
- <https://sites.ualberta.ca/~delliott/ece492/appnotes/2015w/G6_Parsing_JSON_in_C/microjson_tutorial.html>
- <https://sites.ualberta.ca/~delliott/ece492/appnotes/2015w/G6_Parsing_JSON_in_C/microjson_tutorial.zip>
