#!/usr/bin/env bash

set -evxuo pipefail

PROG=api-input

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123","procedencia":"L","holograma":"E","contingencia":true}'

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123","procedencia":"L","holograma":"E","contingencia":false}'

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123","procedencia":"L","holograma":"E","contingencia":"true"}' || true

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123","procedencia":"L","holograma":"E","contingencia":"false"}' || true

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123","procedencia":"L","holograma":"E","contingencia":1}'

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123","procedencia":"L","holograma":"E","contingencia":0}'

./${PROG} '{"fecha":"2024-04-27","placas":"ABC-123"}'

./${PROG} '{}'
