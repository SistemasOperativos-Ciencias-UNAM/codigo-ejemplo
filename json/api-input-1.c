#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "mjson.h"

/* TODO: Add enum for 'procedencia', 'holograma' */

/* datos de entrada */

/* data struct */
struct datos_t {
    char fecha[11];
    char placas[8];
    char procedencia;
    char holograma;
    bool contingencia;
};

/* Parse function to associate JSON fields to struct members */
int json_datos_read(const char *buffer, struct datos_t *datos)
{
    /* Initialize data struct members */
    bzero(datos->fecha, sizeof(datos->fecha));
    bzero(datos->placas, sizeof(datos->placas));
    datos->procedencia = '\0';
    datos->holograma = '\0';
    datos->contingencia = 0;

    /* Mapping of JSON attributes to struct members */
    const struct json_attr_t json_attrs[] = {
        {
            "fecha",
            t_string,
            .addr.string = datos->fecha,
            .len = sizeof(datos->fecha)
        },
        {
            "placas",
            t_string,
            .addr.string = datos->placas,
            .len = sizeof(datos->placas)
        },
        {
            "procedencia",
            t_character,
            .addr.character = &datos->procedencia,
            .dflt.character = '\0'
        },
        {
            "holograma",
            t_character,
            .addr.character = &datos->holograma,
            .dflt.character = '\0'
        },
        {
            "contingencia",
            t_boolean,
            .addr.boolean = &datos->contingencia,
            .dflt.boolean = 0
        },
        /* Terminator */
        {NULL},
    };

    /* Parse the JSON string from buffer */
    return json_read_object(buffer, json_attrs, NULL);
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: ./json_demo_simple <input JSON string>\n");
        return -1;
    }

    /* Allocate space for datos_t */
    struct datos_t *datos = malloc(sizeof(struct datos_t));

    /* Call datos_t parsing function */
    int status = json_datos_read(argv[1], datos);

    if (status == 0) {
        printf("\n\t%s\n\n", "Datos de entrada");
        printf("fecha:\t\t%s\n", datos->fecha);
        printf("placas:\t\t%s\n", datos->placas);
        printf("procedencia:\t%c\n", datos->procedencia);
        printf("holograma:\t%c\n", datos->holograma);
        printf("contingencia:\t%d\n", datos->contingencia);
        printf("contingencia:\t%s\n", datos->contingencia ? "true" : "false");

        if (datos->procedencia == 'L')
            printf("procedencia local\n");
        if (datos->holograma == 'E')
            printf("holograma exento\n");
    }
    else {
        puts(json_error_string(status));
    }

    return status;
}
