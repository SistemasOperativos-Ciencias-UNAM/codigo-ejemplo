/*
  https://www.man7.org/linux/man-pages/man0/sys_types.h.0p.html
  https://www.man7.org/linux/man-pages/man0/time.h.0p.html
  https://www.man7.org/linux/man-pages/man3/time.3p.html
  https://linux.die.net/man/3/asctime
  https://stackoverflow.com/a/15669430
  https://stackoverflow.com/a/73173087
*/

#include <stdio.h>
#include <time.h>

/* Descomentar para imprimir la fecha completa regresada por 'asctime(3)'*/
// #define DEBUG

/* Define el número de segundos en un día */
const int DAY_SECONDS = 86400;

/* Prototipos de función */
void ayer_resta_segundos(void);
void ayer_resta_dia(void);
void imprime_fecha(char identificador[], struct tm var_tm);

/*
  Función principal
*/
int main(void)
{
  /* Método 1: Restar segundos a 'time_t' */
  printf("\n%s\n", "Obteniendo la fecha de ayer restando segundos a 'time_t'");
  ayer_resta_segundos();

  /* Método 2: Restar un dia a 'struct tm' */
  printf("\n%s\n", "Obteniendo la fecha de ayer restando un dia a 'struct tm'");
  ayer_resta_dia();

  return 0;
}

/*
  Obtiene la fecha de ayer restando el número de segundos que hay en un día
  al valor 'time_t' que tiene la fecha de hoy.

  Crea dos instancias de 'time_t' y 'struct tm'
*/
void ayer_resta_segundos(void)
{
  /*
    Obtiene el número de segundos desde UNIX EPOCH: "1970-01-01 00:00:00 +0000 (UTC)"
    El tipo 'time_t' es un entero largo y se define en "sys/types.h"
  */
  time_t hoy_time = time(NULL);
  struct tm hoy_tm = *localtime(&hoy_time);

  /* Se resta el número de segundos que tiene un día para obtener la fecha de ayer */
  time_t ayer_time = hoy_time - DAY_SECONDS;
  struct tm ayer_tm = *localtime(&ayer_time);

  /* Imprime las fechas */
  imprime_fecha("hoy", hoy_tm);
  imprime_fecha("ayer", ayer_tm);
}

/*
  Obtiene la fecha de ayer restando un día al campo 'tm_mday' de 'struct tm' que
  tiene la fecha de hoy.

  Crea una sola instancia de 'time_t' y 'struct tm' que se modifica y al final
  contiene la fecha de ayer.
*/
void ayer_resta_dia(void)
{
  /*
    Obtiene el número de segundos desde UNIX EPOCH: "1970-01-01 00:00:00 +0000 (UTC)"
    El tipo 'time_t' es un entero largo y se define en "sys/types.h"
  */
  time_t hoy_time = time(NULL);
  struct tm mi_tm = *localtime(&hoy_time);

  /* Imprime la fecha de hoy */
  imprime_fecha("hoy", mi_tm);

  /* Resta 1 día para obtener la fecha de ayer */
  mi_tm.tm_mday -= 1;
  mktime(&mi_tm);

  /* Imprime la fecha de ayer */
  imprime_fecha("ayer", mi_tm);
}

/*
  Imprime una fecha y su identificador
*/
void imprime_fecha(char identificador[], struct tm var_tm)
{
  /*
    Las funciones asctime() y  mktme() utilizan la estructura 'tm'
    Esta estructura define el campo 'tm_year' como el número de años desde 1900
    por lo que hay que sumarle este valor para obtener el año actual
  */
  int start_year = 1900;
  #ifdef DEBUG
  printf("%s\t%s", identificador, asctime(&var_tm));
  #endif
  /*
    Se utiliza el especificador de formato para ajustar los valores a 
    2 o 4 dígitos: AAAA-MM-DD
  */
  printf("%s\t%04d-%02d-%02d\n", identificador, var_tm.tm_year+start_year, var_tm.tm_mon, var_tm.tm_mday);
}
