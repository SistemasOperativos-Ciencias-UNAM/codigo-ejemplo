#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

/* Define constantes globales */
#define NON_BLOCK
#define SLEEP
#define BUF_LEN 255

/* Caracter de fin de transmisión */
char EOT[] = { 0x04, 0x00 };

/* Mensaje a imprimir */
char *MENSAJE = "aeiou";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyz";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

/* Imprime un mensaje de error y sale el programa */
void error(char * msg)
{
  perror(msg);
  exit(EXIT_FAILURE);
}

/* Función principal */
int main(void)
{
  /* Crea el par de descriptores de archivo para pipe(2) */
  int pipe_fd[2];
  /* Obtiene el par de descriptores de archivo
     pipe_fd[0]: Lectura
     pipe_fd[1]: Escritura
  */
  /* Crea el pipe */
  if (pipe(pipe_fd) != 0)
    error("Error en pipe(2)");
  
#ifdef NON_BLOCK
  /* Establece los descriptores de archivo del pipe cono NONBLOCK */
  fcntl(pipe_fd[0], F_SETFL, O_NONBLOCK);
  fcntl(pipe_fd[1], F_SETFL, O_NONBLOCK);
#endif

  /* Crea un proceso hijo */
  pid_t pid = fork();

  /* Error en la creación del proceso hijo */
  if (pid < 0)
    error("Error en fork(2)");

  /* Código que ejecuta el proceso padre */
  if (pid > 0)
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getpid();
    pid_t pid_hijo = pid;

    /* Cierra el extremo de lectura del pipe
       Porque el proceso padre solo escribe en el pipe */
    close(pipe_fd[0]);

    /* Abre el descriptor de archivos */
    int pipe_padre_fd = dup(pipe_fd[1]);
    close(pipe_fd[1]);

    /* Inicializa el búffer con '\0' */
    char buffer[BUF_LEN] = "";
    bzero(buffer, BUF_LEN);

    /* Imprime una línea de identificación */
    printf("[padre]\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "[padre]\t\tImprimiendo líneas en: %s\n", "pipe_fd[1]");

    /* Escribe un mensaje de manera incremental en el archivo abierto */
    for(int offset=1 ; offset<=strlen(MENSAJE) ; offset++)
    {
      /* Inicializa el buffer con '\0' */
      bzero(buffer, BUF_LEN);
      /* Copia una parte de MENSAJE hasta offset */
      strncpy(buffer, MENSAJE, offset);
      /* Agrega un retorno de línea al final de la cadena */
      buffer[offset] = '\n';
      /* Escribe en el pipe el bloque de caracteres buffer */
      write(pipe_padre_fd, buffer, BUF_LEN);
    }
    /* Escribe el fin de transmisión en el pipe */
    write(pipe_padre_fd, EOT, sizeof(EOT));

    /* Forza la escritura en todos los archivos abiertos */
    fflush(NULL);
    /* Espera a que el proceso hijo termine */
    wait(NULL);
    /* Cierra el extremo de escritura del pipe */
    close(pipe_padre_fd);
  }

  /* Código que ejecuta el proceso hijo */
  else /* pid == 0 */
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getppid();
    pid_t pid_hijo = getpid();

    /* Inicializa el búffer con '\0' */
    char buffer[BUF_LEN] = "";
    bzero(buffer, BUF_LEN);

    /* Cierra el extremo de escritura
       porque el proceso hijo solo lee del pipe*/
    close(pipe_fd[1]);

    /* Abre el descriptor de archivos */
    int pipe_hijo_fd = dup(pipe_fd[0]);
    close(pipe_fd[0]);

    /* Imprime una línea de identificación */
    printf("[hijo]\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "[hijo]\t\tLeyendo líneas desde: %s\n", "pipe_fd[0]");

    /* Espera a que el proceso padre termine de escribir en el archivo */
    sleep(1);

    /* Lee línea a línea el contenido del archivo abierto */
    int i = 1;
    ssize_t n = read(pipe_hijo_fd, buffer, BUF_LEN);
    while (n > 1)
    {
#ifdef SLEEP
      /* Inserta una pausa */
      sleep(1);
#endif

      /* Imprime el contenido de la línea que se guardó en el búffer */
      printf("[hijo]\tbuffer: (%d)\t%s", i, buffer);
      /* Limpia el buffer despues de usarlo */
      bzero(buffer, BUF_LEN);
      i++;
      /* Lee del pipe el siguiente bloque de caracteres */
      n = read(pipe_hijo_fd, buffer, BUF_LEN);

      /* Termina el ciclo de lectura si lee el caracter de fin de transmisión */
      if (strcmp(buffer, EOT) == 0)
        break;
    }

    /* Cierra el extremo de lectura del pipe */
    close(pipe_hijo_fd);
  }

  return EXIT_SUCCESS;
}
