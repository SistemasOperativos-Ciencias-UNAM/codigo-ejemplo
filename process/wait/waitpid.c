#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void do_sleep(int seconds)
{
  if (seconds<=0)
    return;

  for (int i=0;i<seconds;i++)
  {
    printf(".");
    fflush(NULL);
    sleep(1);
  }
  printf("\n");
}

int main(void)
{
  pid_t pid;
  pid = fork();
  /* Proceso padre */
  if (pid>0)
  {
    printf("Esperando al proceso: %d\n", pid);
    fflush(NULL);
    waitpid(pid,NULL,0);
    printf("El proceso '%d' ha terminado\n", pid);
    fflush(NULL);
  }
  /* Proceso hijo */
  else if (pid==0)
  {
    printf("\t%s\n","Antes de sleep()");
    fflush(NULL);
    do_sleep(10);
    printf("\t%s\n","Despues de sleep()");
    fflush(NULL);
  }
  /* Error */
  else 
  {
    perror("error en fork()");
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}
